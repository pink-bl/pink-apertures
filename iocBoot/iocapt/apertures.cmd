#!../../bin/linux-x86_64/apt

## You may have to change apt to something else
## everywhere it appears in this file

< envPaths

#picsEnvSet("EPICS_CAS_INTF_ADDR_LIST", "172.17.10.48")

cd "${TOP}"

## Register all support components
dbLoadDatabase "dbd/apt.dbd"
apt_registerRecordDeviceDriver pdbbase

## Load record instances
#dbLoadRecords("db/xxx.db","user=epics")

cd "${TOP}/iocBoot/${IOC}"

dbLoadTemplate("aperture.substitutions")

set_savefile_path("/home/epics/EPICS/autosave")
set_pass0_restoreFile("auto_settings.sav")
set_pass1_restoreFile("auto_settings.sav")

iocInit

create_monitor_set("auto_settings.req", 30, "BL=PINK,DEV=AU1,CH1=center, CH2=aperture")

## Start any sequence programs
#seq sncxxx,"user=epics"

epicsThreadSleep 2

dbpf PINK:AU1:apertureX_pini.PROC 1
dbpf PINK:AU1:apertureY_pini.PROC 1
dbpf PINK:AU1:centerX_pini.PROC 1
dbpf PINK:AU1:centerY_pini.PROC 1

dbpf PINK:AU2:apertureX_pini.PROC 1
dbpf PINK:AU2:apertureY_pini.PROC 1
dbpf PINK:AU2:centerX_pini.PROC 1
dbpf PINK:AU2:centerY_pini.PROC 1

dbpf PINK:AU3:apertureX_pini.PROC 1
dbpf PINK:AU3:apertureY_pini.PROC 1
dbpf PINK:AU3:centerX_pini.PROC 1
dbpf PINK:AU3:centerY_pini.PROC 1

epicsThreadSleep 2

dbpf PINK:AU1:apertureX_TS.SCAN ".5 second"
dbpf PINK:AU1:apertureY_TS.SCAN ".5 second"
dbpf PINK:AU1:centerX_TS.SCAN ".5 second"
dbpf PINK:AU1:centerY_TS.SCAN ".5 second"

dbpf PINK:AU2:apertureX_TS.SCAN ".5 second"
dbpf PINK:AU2:apertureY_TS.SCAN ".5 second"
dbpf PINK:AU2:centerX_TS.SCAN ".5 second"
dbpf PINK:AU2:centerY_TS.SCAN ".5 second"

dbpf PINK:AU3:apertureX_TS.SCAN ".5 second"
dbpf PINK:AU3:apertureY_TS.SCAN ".5 second"
dbpf PINK:AU3:centerX_TS.SCAN ".5 second"
dbpf PINK:AU3:centerY_TS.SCAN ".5 second"
